This is a **very** limited SIP server. Currently listens only on UDP, support basic A-B calls and registrations with compile time defined user authentication. The number of users or calls is limited only by the device memory. Keep in mind, there is little to no error handling! Probably I won't continue in development, but feel free to improve the code.

Setup instructions:
  - linux
    - run the ```make -j ssss``` command
    - run with ```./ssss IP_ADDRESS``` command where the ```IP_ADDRESS``` is the device interface IP address
  - ESP32 with Arduino IDE
    - rename the ```src``` folder to ```ssss```
    - rename the ```ssss.cpp``` file to ```ssss.ino```
    - rename the ```md5.c``` file to ```md5.cpp```
    - fill ```WIFI_SSID``` and ```WIFI_PASS``` in ```private_conf.h```

Tested scenarios:
  - registration
    - basic successful registration
    - re-registration
    - deregistration of a registered user
    - deregistration of a not registered user
    - saved user with valid/invalid credentials
    - unsuccessful registration of an unknown user due to forced authentication enabled ```FORCE_AUTH = 1```
  - call
    - A cancels call
    - B with don't disturb set
    - B declines call
    - B accepts call A hangs up
    - B accepts call B hangs up
    - A calls not registered user
    - C calls A or B in an existing A-B call
    - call hold