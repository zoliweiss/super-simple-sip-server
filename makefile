CC = g++
BASEFLAGS = -Wextra -Wpedantic -Wall -O3 -MMD
CFLAGS = $(BASEFLAGS)
CXXFLAGS = $(BASEFLAGS)
VPATH = src
OBJ = md5.o ssss.o

%.o: %.cpp %.c
	$(CC) -c -o $@ $<

ssss: $(OBJ)
	$(CC) -o $@ $^

.PHONY: clean

clean:
	$(RM) *.o *.d ssss

-include $(OBJ:.o=.d)
