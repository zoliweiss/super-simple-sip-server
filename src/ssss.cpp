#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#include <sys/socket.h>
#else
#include <arpa/inet.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <string>
#include <unordered_map>
#include "md5.h"
#include "private_conf.h"
#include "ssss.h"

using string_t = std::string;
using saved_users_t = std::unordered_map<string_t, saved_user_t>;
using registered_users_t = std::unordered_map<string_t, user_t>;
using calls_t = std::unordered_map<string_t, call_t>;
using cache_busy_users_t = std::unordered_map<string_t, string_t>;

registered_users_t g_registered_users;
calls_t g_calls;
saved_users_t g_saved_users;
cache_busy_users_t g_cache_busy_users;
message_t g_received_message;
string_t g_server_ip_address;
int g_socket = 0;

int parse_next_line(const char *packet, const int packet_len, int *packet_iter, int *line_start)
{
  int l_line_len = 0;

  *line_start = *packet_iter;

  if(*packet_iter >= packet_len)
  {
    return 0;
  }

  while(*packet_iter < packet_len
     && 0x0a != packet[*packet_iter])
  {
    (*packet_iter)++;
  }

  l_line_len = *packet_iter - *line_start;
  (*packet_iter)++;

  if(0x0d == packet[*packet_iter - 2])
  {
    l_line_len--;
  }

  return l_line_len;
}

sip_method_t parse_method(const char* line_ptr)
{
  int l_request_method = SIP_REGISTER;

  while('\0' != request_method_string[l_request_method][0])
  {
    if(0 == memcmp(line_ptr, request_method_string[l_request_method], strlen(request_method_string[l_request_method])))
    {
      return (sip_method_t)l_request_method;
    }

    l_request_method++;
  }

  return SIP_NOT_DEF;
}

bool parse_string(const char *line_ptr, const int line_len, const is_quoted_t is_quoted, const char *token, string_t *output)
{
  int l_token_len = strlen(token);
  int l_string_start = 0;

  for(int l_line_elem_iter = 0; l_line_elem_iter < line_len - l_token_len; l_line_elem_iter++)
  {
    if(0 == memcmp(line_ptr + l_line_elem_iter, token, l_token_len))
    {
      l_line_elem_iter += l_token_len + (int)is_quoted;
      l_string_start = l_line_elem_iter;

      for(; l_line_elem_iter < line_len; l_line_elem_iter++)
      {
        if((is_quoted &&
            '"' == line_ptr[l_line_elem_iter]) ||
           (!is_quoted &&
            (',' == line_ptr[l_line_elem_iter] ||
             ';' == line_ptr[l_line_elem_iter] ||
             l_line_elem_iter + 1 == line_len)))
        {
          output->assign(line_ptr + l_string_start, l_line_elem_iter - l_string_start + (int)(l_line_elem_iter + 1 == line_len));

          return true;
        }
      }
    }
  }

  return false;
}

int parse_numeric_header(const char *line_ptr, const int line_len, const int header_len)
{
  char l_temp_str[10] = "";

  memcpy(l_temp_str, line_ptr + header_len, line_len - header_len);
  return atoi(l_temp_str);
}

sip_status_t parse_response_code(const char *line_ptr)
{
  char l_temp_str[10] = "";

  memcpy(l_temp_str, line_ptr + 8, 3);
  return (sip_status_t)atoi(l_temp_str);
}

void parse_cseq(const char *line_ptr, const int line_len, unsigned int *cseq_num, sip_method_t *cseq_method)
{
  int l_line_elem_iter = 0;
  char l_temp_str[10] = "";

  for(l_line_elem_iter = 6; l_line_elem_iter < line_len; l_line_elem_iter++)
  {
    if(' ' == line_ptr[l_line_elem_iter])
    {
      memcpy(l_temp_str, line_ptr + 6, l_line_elem_iter - 6);
      *cseq_num = atoi(l_temp_str);
      break;
    }
  }

  *cseq_method = parse_method(line_ptr + l_line_elem_iter + 1);
}

void parse_via(const char *line_ptr, const int line_len, string_t *via, string_t *branch, int *port)
{
  int l_line_elem_iter = 0;
  char l_temp_str[10] = "";

  for(l_line_elem_iter = 5; l_line_elem_iter < line_len; l_line_elem_iter++)
  {
    if(';' == line_ptr[l_line_elem_iter])
    {
      via->assign(line_ptr + 5, l_line_elem_iter - 5);
      break;
    }
  }

  parse_string(line_ptr, line_len, NOT_QUOTED, "branch=", branch);

  for(unsigned int i = 0; i < via->length(); i++)
  {
    if(via->at(i) == ':')
    {
      memcpy(l_temp_str, via->c_str() + i + 1, via->length() - i - 1);

      *port = atoi(l_temp_str);
      break;
    }
  }
}

void parse_from_to(const char *line_ptr, const int line_len, string_t *username, string_t *tag)
{
  int l_line_elem_iter = 0;

  for(; l_line_elem_iter < line_len - 5; l_line_elem_iter++)
  {
    if(0 == memcmp(line_ptr + l_line_elem_iter, "sip:", 4))
    {
      int l_username_start = l_line_elem_iter + 4;

      for(; l_line_elem_iter < line_len; l_line_elem_iter++)
      {
        if('@' == line_ptr[l_line_elem_iter])
        {
          break;
        }
      }

      username->assign(line_ptr + l_username_start, l_line_elem_iter - l_username_start);
      break;
    }
  }

  parse_string(line_ptr, line_len, NOT_QUOTED, "tag=", tag);
}

void parse_authorization(const char *line_ptr, const int line_len, string_t *nonce, string_t *uri, string_t *nc, string_t *cnonce, string_t *response, string_t *opaque)
{
  parse_string(line_ptr, line_len, QUOTED, "nonce=", nonce);
  parse_string(line_ptr, line_len, QUOTED, "uri=", uri);
  parse_string(line_ptr, line_len, NOT_QUOTED, "nc=", nc);
  parse_string(line_ptr, line_len, QUOTED, "cnonce=", cnonce);
  parse_string(line_ptr, line_len, QUOTED, "response=", response);
  parse_string(line_ptr, line_len, QUOTED, "opaque=", opaque);
}

void parse_payload(const char *packet, const int packet_len, const int content_len, char *payload)
{
  int l_payload_elem_iter = 0;

  for(; l_payload_elem_iter < packet_len; l_payload_elem_iter++)
  {
    if(0 == memcmp(packet + l_payload_elem_iter, "\x0d\x0a\x0d\x0a", 4) &&
       l_payload_elem_iter + content_len + 4 <= packet_len)
    {
      memcpy(payload, packet + l_payload_elem_iter + 4, content_len);
      break;
    }
  }
}

void parse_packet(const char *packet, const int packet_len, message_t *received_message)
{
  int l_packet_iter = 0;
  int l_line_len = 0;
  int l_line_start = 0;
  sip_method_t l_temp_res = SIP_NOT_DEF;

  received_message->init();

  while(0 != (l_line_len = parse_next_line(packet, packet_len, &l_packet_iter, &l_line_start)))
  {
    char l_empty_line[1000] = "";
    memcpy(l_empty_line, packet + l_line_start, l_line_len);

    DEBUG(KMAG, "%s\n", l_empty_line);

    const char *l_line_ptr = packet + l_line_start;

    if(SIP_NOT_DEF == received_message->method &&
       SIP_STATUS_NOT_DEF == received_message->response_code &&
       SIP_NOT_DEF != (l_temp_res = parse_method(l_line_ptr)))
    {
      received_message->method = l_temp_res;
    }
    else if(SIP_NOT_DEF == received_message->method &&
            SIP_STATUS_NOT_DEF == received_message->response_code &&
            0 == memcmp(l_line_ptr, "SIP", 3))
    {
      received_message->response_code = parse_response_code(l_line_ptr);
    }
    else if(0 == memcmp(l_line_ptr, "Via:", 4))
    {
      parse_via(l_line_ptr, l_line_len, &received_message->via, &received_message->branch, &received_message->port);
    }
    else if(0 == memcmp(l_line_ptr, "From:", 5))
    {
      parse_from_to(l_line_ptr, l_line_len, &received_message->from_name, &received_message->from_tag);
    }
    else if(0 == memcmp(l_line_ptr, "To:", 3))
    {
      parse_from_to(l_line_ptr, l_line_len, &received_message->to_name, &received_message->to_tag);
    }
    else if(0 == memcmp(l_line_ptr, "Call-ID:", 8))
    {
      received_message->call_id.assign(l_line_ptr + 9, l_line_len - 9);
    }
    else if(0 == memcmp(l_line_ptr, "Content-Type:", 13))
    {
      received_message->content_type.assign(l_line_ptr + 14, l_line_len - 14);
    }
    else if(0 == memcmp(l_line_ptr, "CSeq:", 5))
    {
      parse_cseq(l_line_ptr, l_line_len, &received_message->cseq_num, &received_message->cseq_method);
    }
    else if(0 == memcmp(l_line_ptr, "Content-Length:", 15))
    {
      received_message->content_len = parse_numeric_header(l_line_ptr, l_line_len, 15);
    }
    else if(0 == memcmp(l_line_ptr, "Expires:", 8))
    {
      received_message->expires = parse_numeric_header(l_line_ptr, l_line_len, 8);
    }
    else if(0 == memcmp(l_line_ptr, "Authorization:", 14))
    {
      parse_authorization(l_line_ptr,
                          l_line_len,
                          &received_message->nonce,
                          &received_message->uri,
                          &received_message->nc,
                          &received_message->cnonce,
                          &received_message->response,
                          &received_message->opaque);
    }
  }

  if(0 != received_message->content_len)
  {
    parse_payload(packet, packet_len, received_message->content_len, received_message->payload);
  }
}

bool validate_ip_address(char const *ip_address)
{
  unsigned int l_ip_address_len = strlen(ip_address);
  unsigned int l_a = 0;
  unsigned int l_b = 0;
  unsigned int l_c = 0;
  unsigned int l_d = 0;

  if(15 < l_ip_address_len ||
      7 > l_ip_address_len ||
      4 != sscanf(ip_address, "%u.%u.%u.%u", &l_a, &l_b, &l_c, &l_d) ||
      255 < l_a ||
      255 < l_b ||
      255 < l_c ||
      255 < l_d)
  {
    return false;
  }

  return true;
}

#ifdef ARDUINO_ARCH_ESP32
void connect_to_wifi()
{
  delay(10);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASS);

  while(WL_CONNECTED != WiFi.status())
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.println("WiFi is connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.print("Signal strength (RSSI): ");
  Serial.print(WiFi.RSSI());
  Serial.println(" dBm");
}

void set_server_ip_address(string_t *server_ip_address)
{
  *server_ip_address = WiFi.localIP().toString().c_str();
}

#else
void set_server_ip_address(int argc, char const *argv[], string_t *server_ip_address)
{
  if(2 == argc &&
     validate_ip_address(argv[1]))
  {
    *server_ip_address = argv[1];
  }
  else
  {
    printf("Please specify the IP address of the SIP server\ne.g. 'ssss 192.168.0.1'\n");
    exit(5);
  }
}
#endif

bool validate_credentials(const message_t &received_message, const saved_user_t *saved_user, const string_t &server_ip_address)
{
  string_t l_final_str = "";
  string_t l_a1_str = "";
  string_t l_a2_str = "";
  string_t l_final_md5_hash= "";
  string_t l_a1_md5_hash = "";
  string_t l_a2_md5_hash = "";

  l_a1_str = saved_user->auth_name + ":" + server_ip_address + ":" + saved_user->password;
  l_a2_str = "REGISTER:" + received_message.uri;

  MD5(l_a1_str, &l_a1_md5_hash);
  MD5(l_a2_str, &l_a2_md5_hash);

  l_final_str = l_a1_md5_hash + ":" + received_message.nonce + ":" + received_message.nc + ":" + received_message.cnonce + ":auth:" + l_a2_md5_hash;

  MD5(l_final_str, &l_final_md5_hash);

  if(l_final_md5_hash == received_message.response &&
     saved_user->nonce == received_message.nonce &&
     saved_user->opaque == received_message.opaque)
  {
    return true;
  }

  return false;
}

void read_saved_users_from_config(saved_users_t *saved_users)
{
  /* this is a dirty hack, but there is no easy way to store configuration on ESP32 */
  saved_user_t l_saved_user;

  l_saved_user.username = "5000";
  l_saved_user.auth_name = "5000_auth";
  l_saved_user.password = "5000_pass";
  l_saved_user.nonce = "";
  l_saved_user.opaque = "";

  try
  {
    saved_users->insert({l_saved_user.username, l_saved_user});
  }
  catch(const std::bad_alloc&)
  {
    return;
  }
}

saved_user_t* get_saved_user_by_username(const string_t &username, saved_users_t *saved_users)
{
  auto search = saved_users->find(username);

  if(search != saved_users->end())
  {
    return &search->second;
  }

  return NULL;
}

void clear_saved_user_temp_data(saved_user_t *saved_user)
{
  saved_user->nonce = "";
  saved_user->opaque = "";
}

user_t* get_user_by_username(const string_t &username, registered_users_t *registered_users)
{
  auto search = registered_users->find(username);

  if(search != registered_users->end())
  {
    return &search->second;
  }

  return NULL;
}

sip_status_t register_user(const message_t &received_message, registered_users_t *registered_users)
{
  user_t l_registered_user;
  user_t *l_existing_user = NULL;

#ifdef ARDUINO_ARCH_ESP32
  if(HEAP_LIMIT > ESP.getFreeHeap())
  {
    return SIP_SERVICE_UNAVAIL;
  }
#endif

  l_registered_user.ip_address = 0;
  l_registered_user.port = 0;

  if(NULL != (l_existing_user = get_user_by_username(received_message.from_name, registered_users)))
  {
    l_existing_user->ip_address = received_message.sender_ip_address;
    l_existing_user->port = received_message.port;

    return SIP_OK;
  }

  l_registered_user.ip_address = received_message.sender_ip_address;
  l_registered_user.port = received_message.port;

  try
  {
    registered_users->insert({received_message.from_name, l_registered_user});
  }
  catch(const std::bad_alloc&)
  {
    return SIP_SERVICE_UNAVAIL;
  }

  return SIP_OK;
}

sip_status_t deregister_user(const message_t &received_message, registered_users_t *registered_users)
{
  auto search = registered_users->find(received_message.from_name);

  if(search != registered_users->end())
  {
    registered_users->erase(search);

    return SIP_OK;
  }

  return SIP_NOT_FOUND;
}

bool is_callers_ip_address_valid(const unsigned int ip_address, const string_t &username, registered_users_t *registered_users)
{
  user_t *l_existing_user = NULL;

  if(NULL != (l_existing_user = get_user_by_username(username, registered_users)) &&
     l_existing_user->ip_address == ip_address)
  {
    return true;
  }

  return false;
}

user_t* get_other_party(const struct sockaddr_in &sender_address, const call_t *call, registered_users_t *registered_users)
{
  user_t *l_existing_user = NULL;

  if(NULL != (l_existing_user = get_user_by_username(call->cgp_username, registered_users)) &&
     l_existing_user->ip_address == sender_address.sin_addr.s_addr &&
     l_existing_user->port == htons(sender_address.sin_port))
  {
    return get_user_by_username(call->cdp_username, registered_users);
  }
  else
  {
    return l_existing_user;
  }
}

string_t deparse_response_code(const sip_status_t response_code)
{
  switch(response_code)
  {
    case SIP_TRYING:
      return "Trying";
    case SIP_RINGING:
      return "Ringing";
    case SIP_OK:
      return "OK";
    case SIP_UNAUTHORIZED:
      return "Unauthorized";
    case SIP_FORBIDDEN:
      return "Forbidden";
    case SIP_NOT_FOUND:
      return "Not Found";
    case SIP_TEMP_UNAVAIL:
      return "Temporarily Unavailable";
    case SIP_CALL_NOT_EXIST:
      return "call Does Not Exist";
    case SIP_BUSY_HERE:
      return "Busy Here";
    case SIP_REQUEST_TERM:
      return "Request Terminated";
    case SIP_NOT_ACC_HERE:
      return "Not Acceptable Here";
    case SIP_SERVER_ERROR:
      return "Server Internal Error";
    case SIP_NOT_IMPLEMENT:
      return "Not Implemented";
    case SIP_SERVICE_UNAVAIL:
      return "Service Unavailable";
    case SIP_DECLINE:
      return "Decline";
    case SIP_NOT_DEF:
      return "Kiskutya";
  }

  return "Kiskutya";
}

int deparse_response(const sip_status_t response_code, const message_t &received_message, const string_t &server_ip_address, saved_users_t *saved_users, char *response_buffer)
{
  char l_to_tag[100] = "";
  char l_www_authenticate[200] = "";
  char l_nonce[12] = "";
  char l_opaque[12] = "";
  saved_user_t *l_saved_user_ptr = NULL;

  memset(response_buffer, 0, BUFF_SIZE);

  if(SIP_TRYING != response_code &&
     received_message.to_tag.empty())
  {
    sprintf(l_to_tag, ";tag=%d", rand());
  }
  else if(!received_message.to_tag.empty())
  {
    sprintf(l_to_tag, ";tag=%.48s", received_message.to_tag.c_str());
  }

  if(SIP_UNAUTHORIZED == response_code &&
     NULL != (l_saved_user_ptr = get_saved_user_by_username(received_message.from_name, saved_users)))
  {
    sprintf(l_nonce, "%d", rand());
    sprintf(l_opaque, "%d", rand());

    l_saved_user_ptr->nonce = l_nonce;
    l_saved_user_ptr->opaque = l_opaque;

    sprintf(l_www_authenticate,
      "WWW-Authenticate: Digest \
realm=\"%.16s\", \
qop=\"auth\", \
algorithm=MD5, \
nonce=\"%s\", \
opaque=\"%s\"\r\n",
      server_ip_address.c_str(),
      l_nonce,
      l_opaque);
  }

  int l_response_len = sprintf(response_buffer,
    "SIP/2.0 %d %s\r\n\
Via: %.100s;branch=%.100s\r\n\
From: <sip:%.20s@%.16s>;tag=%.48s\r\n\
To: <sip:%.20s@%.16s>%.48s\r\n\
Call-ID: %.100s\r\n\
CSeq: %d %s\r\n\
%s\
Content-Length: 0\r\n\
\r\n",
    response_code, deparse_response_code(response_code).c_str(),
    received_message.via.c_str(), received_message.branch.c_str(),
    received_message.from_name.c_str(), server_ip_address.c_str(), received_message.from_tag.c_str(),
    received_message.to_name.c_str(), server_ip_address.c_str(), l_to_tag,
    received_message.call_id.c_str(),
    received_message.cseq_num, request_method_string[received_message.cseq_method],
    l_www_authenticate);

  return l_response_len;
}

int deparse_relay(const string_t &server_ip_address, message_t *received_message, char *response_buffer)
{
  char l_to_tag[100] = "";
  char l_content_type[36] = "";
  int l_response_len = 0;

  memset(response_buffer, 0, BUFF_SIZE);

  if(!received_message->to_tag.empty())
  {
    sprintf(l_to_tag, ";tag=%.48s", received_message->to_tag.c_str());
  }

  if(0 != received_message->content_len &&
     !received_message->content_type.empty() &&
     received_message->content_len == strlen(received_message->payload))
  {
    sprintf(l_content_type, "Content-Type: %.20s\r\n", received_message->content_type.c_str());
  }
  else
  {
    received_message->content_len = 0;
    memset(received_message->payload, 0, sizeof(received_message->payload));
  }

  if(SIP_NOT_DEF == received_message->method &&
     SIP_STATUS_NOT_DEF != received_message->response_code)
  {
    l_response_len = sprintf(response_buffer,
      "SIP/2.0 %d %s\r\n",
      received_message->response_code, deparse_response_code(received_message->response_code).c_str());
  }
  else
  {
    l_response_len = sprintf(response_buffer,
      "%s SIP:%.20s@%.16s SIP/2.0\r\n",
      request_method_string[received_message->method], received_message->to_name.c_str(), server_ip_address.c_str());
  }

  l_response_len += sprintf(response_buffer + l_response_len,
    "Via: SIP/2.0/UDP %.16s:%d;branch=%.100s\r\n\
From: <sip:%.20s@%.16s>;tag=%.48s\r\n\
To: <sip:%.20s@%.16s>%s\r\n\
Call-ID: %.100s\r\n\
Max-Forwards: 70\r\n\
CSeq: %d %s\r\n\
Allow: INVITE, ACK, BYE, CANCEL\r\n\
Contact: <sip:%.20s@%.16s:%d>\r\n\
%s\
Content-Length: %d\r\n\
\r\n\
%s",
    server_ip_address.c_str(), SERVER_PORT, received_message->branch.c_str(),
    received_message->from_name.c_str(), server_ip_address.c_str(), received_message->from_tag.c_str(),
    received_message->to_name.c_str(), server_ip_address.c_str(), l_to_tag,
    received_message->call_id.c_str(),
    received_message->cseq_num, request_method_string[received_message->cseq_method],
    received_message->from_name.c_str(), server_ip_address.c_str(), SERVER_PORT,
    l_content_type,
    received_message->content_len,
    received_message->payload);

  return l_response_len;
}

sip_status_t handle_saved_users(const message_t &received_message, const string_t &server_ip_address, saved_users_t *saved_users)
{
  saved_user_t *l_saved_user_ptr = NULL;

  if(NULL != (l_saved_user_ptr = get_saved_user_by_username(received_message.from_name, saved_users)))
  {
    if(l_saved_user_ptr->nonce.empty())
    {
      return SIP_UNAUTHORIZED;
    }
    else if(!validate_credentials(received_message, l_saved_user_ptr, server_ip_address))
    {
      clear_saved_user_temp_data(l_saved_user_ptr);

      return SIP_FORBIDDEN;
    }

    clear_saved_user_temp_data(l_saved_user_ptr);
  }
  else if(FORCE_AUTH)
  {
    return SIP_FORBIDDEN;
  }

  return SIP_STATUS_NOT_DEF;
}

bool is_user_busy(const string_t &username, const cache_busy_users_t &cache_busy_users)
{
  if(cache_busy_users.find(username) == cache_busy_users.end())
  {
    return false;
  }

  return true;
}

void add_busy_users_to_cache(const message_t &received_message, cache_busy_users_t *cache_busy_users)
{
  cache_busy_users->insert({received_message.from_name, received_message.call_id});
  cache_busy_users->insert({received_message.to_name, received_message.call_id});
}

void remove_busy_users_from_cache(const call_t *call, cache_busy_users_t *cache_busy_users)
{
  if(NULL == call)
  {
    return;
  }

  cache_busy_users->erase(call->cdp_username);
  cache_busy_users->erase(call->cgp_username);
}

void remove_call(const call_t *call, calls_t *calls, cache_busy_users_t *cache_busy_users)
{
  if(NULL == call)
  {
    return;
  }

  remove_busy_users_from_cache(call, cache_busy_users);

  calls->erase(call->call_id);
  WARN("call %s removed, number of calls: %lu", call->call_id.c_str(), calls->size());
}

call_t* add_call(const message_t &received_message, calls_t *calls, cache_busy_users_t *cache_busy_users)
{
  call_t l_call;

#ifdef ARDUINO_ARCH_ESP32
  if(HEAP_LIMIT > ESP.getFreeHeap())
  {
    return NULL;
  }
#endif

  l_call.state = PRE_ALERTING;
  l_call.cgp_username = received_message.from_name;
  l_call.cdp_username = received_message.to_name;
  l_call.from_tag = received_message.from_tag;
  l_call.to_tag = received_message.to_tag;
  l_call.call_id = received_message.call_id;

  try
  {
    auto l_result = calls->insert({received_message.call_id, l_call});

    add_busy_users_to_cache(received_message, cache_busy_users);

    return &l_result.first->second;
  }
  catch(const std::bad_alloc&)
  {
    return NULL;
  }
}

bool is_message_in_dialog(const message_t &received_message, const call_t *call)
{
  if(received_message.is_request())
  {
    if(SIP_CANCEL == received_message.method)
    {
      if(received_message.from_tag == call->from_tag)
      {
        return true;
      }
    }
    else
    {
      if((received_message.from_name == call->cdp_username &&
          received_message.to_tag == call->from_tag &&
          received_message.from_tag == call->to_tag) ||
         (received_message.from_name == call->cgp_username &&
          received_message.from_tag == call->from_tag &&
          received_message.to_tag == call->to_tag))
      {
        return true;
      }
    }
  }
  else if(received_message.from_tag == call->last_request_from_tag &&
          (PRE_ALERTING == call->state ||
           received_message.to_tag == call->last_request_to_tag))
  {
    return true;
  }

  return false;
}

call_t* get_call_by_call_id(const string_t &call_id, calls_t *calls)
{
  auto l_search = calls->find(call_id);

  if(l_search != calls->end())
  {
    return &l_search->second;
  }

  return NULL;
}

call_t* get_call_by_username(const cache_busy_users_t &cache_busy_users, const string_t &username, calls_t *calls)
{
  auto l_search_busy_users = cache_busy_users.begin();
  auto l_search_calls = calls->begin();

  if(cache_busy_users.end() != (l_search_busy_users = cache_busy_users.find(username)) &&
     calls->end() != (l_search_calls = calls->find(l_search_busy_users->second)))
  {
    return &l_search_calls->second;
  }

  return NULL;
}

int bind_socket()
{
  struct sockaddr_in l_si_server;
  int l_socket = 0;

  memset(&l_si_server, 0, sizeof(l_si_server));

  l_si_server.sin_family = AF_INET;
  l_si_server.sin_port = htons(SERVER_PORT);
  l_si_server.sin_addr.s_addr = htonl(INADDR_ANY);

  if(-1 == (l_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)))
  {
    exit(1);
  }

  if(-1 == bind(l_socket, (struct sockaddr*)&l_si_server, sizeof(l_si_server)))
  {
    exit(2);
  }

  return l_socket;
}

void receive_packet(const int socket, int *packet_len, char *packet, struct sockaddr_in *si_other, unsigned int *slen)
{
  if(-1 == (*packet_len = recvfrom(socket, packet, BUFF_SIZE, 0, (struct sockaddr*)si_other, slen)))
  {
    exit(3);
  }

  DEBUG_TIME(KNRM, "Received packet from %s:%d", inet_ntoa(si_other->sin_addr), ntohs(si_other->sin_port));
}

void send_packet(const int socket, const char *response_buffer, const int response_len, struct sockaddr_in *si_other, unsigned int slen)
{
  if(-1 == sendto(socket, response_buffer, response_len, 0, (struct sockaddr*)si_other, slen))
  {
    exit(4);
  }
}

#ifdef ARDUINO_ARCH_ESP32
void setup()
{
  Serial.begin(115200);
  connect_to_wifi();
  set_server_ip_address(&g_server_ip_address);
#else
int main(int argc, char const *argv[])
{
#endif

  srand(time(NULL));
  g_socket = bind_socket();
  read_saved_users_from_config(&g_saved_users);

#ifdef ARDUINO_ARCH_ESP32
}

void loop()
#else
  set_server_ip_address(argc, argv, &g_server_ip_address);
  while(true)
#endif
{
    struct sockaddr_in l_si_other;
    unsigned int l_slen = sizeof(l_si_other);
    call_t *l_idle_call_ptr = NULL;
    call_t *l_call_ptr = NULL;
    user_t *l_other_party_ptr = NULL;
    int l_packet_len = 0;
    int l_response_len = 0;
    sip_status_t l_response = SIP_STATUS_NOT_DEF;
    char l_packet[BUFF_SIZE];
    char l_response_buffer[BUFF_SIZE];
    bool l_relay_needed = false;
    bool l_is_message_in_dialog = false;

    receive_packet(g_socket, &l_packet_len, l_packet, &l_si_other, &l_slen);
    parse_packet(l_packet, l_packet_len, &g_received_message);
    g_received_message.sender_ip_address = l_si_other.sin_addr.s_addr;

    if(NULL != (l_call_ptr = get_call_by_call_id(g_received_message.call_id, &g_calls)))
    {
      l_is_message_in_dialog = is_message_in_dialog(g_received_message, l_call_ptr);
    }

    if(g_received_message.is_request())
    {
      switch(g_received_message.method)
      {
        case SIP_REGISTER:
          if(SIP_STATUS_NOT_DEF == (l_response = handle_saved_users(g_received_message, g_server_ip_address, &g_saved_users)))
          {
            if(0 == g_received_message.expires)
            {
              l_response = deregister_user(g_received_message, &g_registered_users);

              if(SIP_OK == l_response)
              {
                l_idle_call_ptr = get_call_by_username(g_cache_busy_users, g_received_message.from_name, &g_calls);

                INFO("user %s removed, number of users: %lu", g_received_message.from_name.c_str(), g_registered_users.size());
              }
            }
            else
            {
              l_response = register_user(g_received_message, &g_registered_users);

              if(SIP_OK == l_response)
              {
                INFO("user %s added, number of users: %lu", g_received_message.from_name.c_str(), g_registered_users.size());
              }
            }
          }
          break;

        case SIP_INVITE:
          if(NULL == get_user_by_username(g_received_message.from_name, &g_registered_users) ||
             !is_callers_ip_address_valid(g_received_message.sender_ip_address, g_received_message.from_name, &g_registered_users))
          {
            l_response = SIP_FORBIDDEN;
          }
          else if(NULL == get_user_by_username(g_received_message.to_name, &g_registered_users))
          {
            l_response = SIP_NOT_FOUND;
          }
          else if(l_is_message_in_dialog)
          {
            l_relay_needed = true;
          }
          else if(g_received_message.from_name == g_received_message.to_name ||
                  is_user_busy(g_received_message.from_name, g_cache_busy_users) ||
                  is_user_busy(g_received_message.to_name, g_cache_busy_users))
          {
            l_response = SIP_BUSY_HERE;
          }
          else if(NULL != (l_call_ptr = add_call(g_received_message, &g_calls, &g_cache_busy_users)))
          {
            l_response = SIP_TRYING;
            l_relay_needed = true;

            INFO("call %s attempt from: %s to: %s", l_call_ptr->call_id.c_str(), l_call_ptr->cgp_username.c_str(), l_call_ptr->cdp_username.c_str());
          }
          else
          {
            l_response = SIP_SERVICE_UNAVAIL;
          }
          break;

        case SIP_BYE:
          if(l_is_message_in_dialog)
          {
            l_response = SIP_OK;
            l_relay_needed = true;

            l_idle_call_ptr = l_call_ptr;
            INFO("call %s released", l_call_ptr->call_id.c_str());
          }
          else
          {
            l_response = SIP_CALL_NOT_EXIST;
          }
          break;

        case SIP_CANCEL:
          if(l_is_message_in_dialog &&
             ALERTING == l_call_ptr->state)
          {
            l_response = SIP_OK;
            l_relay_needed = true;
          }
          else if(l_is_message_in_dialog &&
                  PRE_ALERTING == l_call_ptr->state)
          {
            l_response = SIP_REQUEST_TERM;
            l_idle_call_ptr = l_call_ptr;
            INFO("call %s cancelled", l_call_ptr->call_id.c_str());
          }
          else
          {
            l_response = SIP_CALL_NOT_EXIST;
          }
          break;

        case SIP_ACK:
          if(l_is_message_in_dialog)
          {
            l_relay_needed = true;

            if(CANCEL_PENDING == l_call_ptr->state)
            {
              l_idle_call_ptr = l_call_ptr;
            }
          }
          break;

        default:
          l_response = SIP_NOT_IMPLEMENT;
          break;
      }

      if(SIP_CANCEL != g_received_message.method &&
         NULL != l_call_ptr)
      {
        l_call_ptr->last_request_from_tag = g_received_message.from_tag;
        l_call_ptr->last_request_to_tag = g_received_message.to_tag;
      }
    }
    else if(g_received_message.is_valid_response() &&
            l_is_message_in_dialog)
    {
      l_relay_needed = true;

      if(!g_received_message.to_tag.empty() &&
         l_call_ptr->to_tag.empty())
      {
        l_call_ptr->to_tag = g_received_message.to_tag;
        l_call_ptr->last_request_to_tag = g_received_message.to_tag;
      }

      switch(g_received_message.response_code)
      {
        case SIP_RINGING:
          l_call_ptr->state = ALERTING;
          break;

        case SIP_OK:
          switch(g_received_message.cseq_method)
          {
            case SIP_INVITE:
              if(ACTIVE != l_call_ptr->state)
              {
                l_call_ptr->state = ACTIVE;
                INFO("call %s answered, number of calls: %lu", l_call_ptr->call_id.c_str(), g_calls.size());
              }
              break;

            default:
              l_relay_needed = false;
              break;
          }
          break;

        case SIP_REQUEST_TERM:
          INFO("call %s cancelled", l_call_ptr->call_id.c_str());
          l_call_ptr->state = CANCEL_PENDING;
          break;

        case SIP_BUSY_HERE:
        case SIP_TEMP_UNAVAIL:
        case SIP_DECLINE:
          INFO("call %s declined", l_call_ptr->call_id.c_str());
          l_call_ptr->state = CANCEL_PENDING;
          break;

        default:
          break;
      }
    }

    if(SIP_STATUS_NOT_DEF != l_response)
    {
      l_response_len = deparse_response(l_response, g_received_message, g_server_ip_address, &g_saved_users, l_response_buffer);
      send_packet(g_socket, l_response_buffer, l_response_len, &l_si_other, l_slen);

      DEBUG_TIME(KNRM, "Sent response to %s:%d", inet_ntoa(l_si_other.sin_addr), ntohs(l_si_other.sin_port));
      DEBUG(KCYN, "%s", l_response_buffer);
    }

    if(l_relay_needed)
    {
      l_other_party_ptr = get_other_party(l_si_other, l_call_ptr, &g_registered_users);

      l_si_other.sin_port = htons(l_other_party_ptr->port);
      l_si_other.sin_addr.s_addr = l_other_party_ptr->ip_address;

      l_response_len = deparse_relay(g_server_ip_address, &g_received_message, l_response_buffer);
      send_packet(g_socket, l_response_buffer, l_response_len, &l_si_other, l_slen);

      DEBUG_TIME(KNRM, "Relayed message to %s:%d", inet_ntoa(l_si_other.sin_addr), ntohs(l_si_other.sin_port));
      DEBUG(KRED, "%s", l_response_buffer);
    }

    remove_call(l_idle_call_ptr, &g_calls, &g_cache_busy_users);
  }

#ifndef ARDUINO_ARCH_ESP32
  return 0;
}
#endif
