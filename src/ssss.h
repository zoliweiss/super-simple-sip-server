/* application definitions */
#define BUFF_SIZE    2048
#define SERVER_PORT  5060
#define DEBUGLEVEL   2
#define HEAP_LIMIT   100000
#define FORCE_AUTH   0

/* printf colour */
#ifdef ARDUINO_ARCH_ESP32
#define KNRM  ""
#define KRED  ""
#define KGRN  ""
#define KYEL  ""
#define KMAG  ""
#define KCYN  ""
#else
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#endif

/* macros */
#define  INFO(_msg_, ...)  printf(KGRN "[INFO] ");printf(KNRM _msg_"\n", ##__VA_ARGS__)

#if DEBUGLEVEL == 2
  time_t g_time;
  #define WARN(_msg_, ...)          printf(KYEL "[WARN] ");printf(KNRM _msg_"\n", ##__VA_ARGS__)
  #define DEBUG(_color_, ...)       printf(_color_ __VA_ARGS__)
  #define DEBUG_TIME(_color_, ...)  printf(_color_ __VA_ARGS__);time(&g_time);printf(" @ %s",ctime(&g_time))
  #define LOG_VAR(_var_)            printf(KNRM "Line %d ",__LINE__);printf(#_var_);for(unsigned int i=0;i<sizeof(_var_);i++) printf(" %02x",((unsigned char*)&_var_)[i]);printf("\n")
  #define LOG_PTR(_ptr_, _size_)    printf(KNRM "Line %d ",__LINE__);printf(#_ptr_);if(NULL==_ptr_){printf(" NULL");}else{for(unsigned int i=0;i<_size_;i++)printf(" %02x",((unsigned char*)_ptr_)[i]);}printf("\n")
#elif DEBUGLEVEL == 1
  #define WARN(_msg_, ...)          printf(KYEL "[WARN] ");printf(KNRM _msg_"\n", ##__VA_ARGS__)
  #define DEBUG(...)
  #define DEBUG_TIME(...)
  #define LOG_VAR(...)
  #define LOG_PTR(...)
#else
  #define WARN(...)
  #define DEBUG(...)
  #define DEBUG_TIME(...)
  #define LOG_VAR(...)
  #define LOG_PTR(...)
#endif

/* data types */
static const char *request_method_string[] = {
  "NOT DEF",
  "REGISTER",
  "INVITE",
  "CANCEL",
  "BYE",
  "ACK",
  "SUBSCRIBE",
  "INFO",
  "OPTIONS",
  "PRACK",
  "NOTIFY",
  "PUBLISH",
  "REFER",
  "MESSAGE",
  "UPDATE",
  ""
};

enum is_quoted_t
{
  QUOTED     = true,
  NOT_QUOTED = false
};

enum sip_method_t
{
  SIP_NOT_DEF   = 0,
  SIP_REGISTER  = 1,
  SIP_INVITE    = 2,
  SIP_CANCEL    = 3,
  SIP_BYE       = 4,
  SIP_ACK       = 5,
  SIP_SUBSCRIBE = 6,
  SIP_INFO      = 7,
  SIP_OPTIONS   = 8,
  SIP_PRACK     = 9,
  SIP_NOTIFY    = 10,
  SIP_PUBLISH   = 11,
  SIP_REFER     = 12,
  SIP_MESSAGE   = 13,
  SIP_UPDATE    = 14
};

enum call_state_t{
  NOT_DEF        = 0,
  IDLE           = 1,
  PRE_ALERTING   = 2,
  ALERTING       = 3,
  ACTIVE         = 4,
  CANCEL_PENDING = 5
};

enum sip_status_t
{
  SIP_STATUS_NOT_DEF  = 0,
  SIP_RESP_MIN        = 100,
  SIP_TRYING          = 100,
  SIP_RINGING         = 180,
  SIP_OK              = 200,
  SIP_UNAUTHORIZED    = 401,
  SIP_FORBIDDEN       = 403,
  SIP_NOT_FOUND       = 404,
  SIP_TEMP_UNAVAIL    = 480,
  SIP_CALL_NOT_EXIST  = 481,
  SIP_BUSY_HERE       = 486,
  SIP_REQUEST_TERM    = 487,
  SIP_NOT_ACC_HERE    = 488,
  SIP_SERVER_ERROR    = 500,
  SIP_NOT_IMPLEMENT   = 501,
  SIP_SERVICE_UNAVAIL = 503,
  SIP_DECLINE         = 603,
  SIP_RESP_MAX        = 603
};

struct message_t
{
  sip_method_t method;
  unsigned int cseq_num;
  sip_method_t cseq_method;
  std::string from_name;
  std::string to_name;
  std::string from_tag;
  std::string to_tag;
  std::string via;
  std::string call_id;
  std::string branch;
  std::string nonce;
  std::string uri;
  std::string nc;
  std::string cnonce;
  std::string response;
  std::string opaque;
  std::string content_type;
  unsigned int expires;
  unsigned int content_len;
  unsigned int sender_ip_address;
  int port;
  sip_status_t response_code;
  char payload[1000];

  inline bool is_request() const
  {
    return SIP_NOT_DEF != method &&
           SIP_STATUS_NOT_DEF == response_code;
  }

  inline bool is_valid_response() const
  {
    return SIP_RESP_MIN < response_code &&
           SIP_RESP_MAX >= response_code;
  }

  void init()
  {
    method = SIP_NOT_DEF;
    cseq_num = 0;
    cseq_method = SIP_NOT_DEF;
    from_name = "";
    to_name = "";
    from_tag = "";
    to_tag = "";
    via = "";
    call_id = "";
    branch = "";
    nonce = "";
    uri = "";
    nc = "";
    cnonce = "";
    response = "";
    opaque = "";
    content_type = "";
    expires = 0;
    content_len = 0;
    sender_ip_address = 0;
    port = 0;
    response_code = SIP_STATUS_NOT_DEF;

    memset(payload, 0, sizeof(payload));
  }
};

struct user_t
{
  unsigned int ip_address;
  int port;
};

struct call_t
{
  call_state_t state;
  std::string call_id;
  std::string cdp_username;
  std::string cgp_username;
  std::string from_tag;
  std::string to_tag;
  std::string last_request_from_tag;
  std::string last_request_to_tag;
};

struct saved_user_t
{
  std::string username;
  std::string auth_name;
  std::string password;
  std::string nonce;
  std::string opaque;
};
